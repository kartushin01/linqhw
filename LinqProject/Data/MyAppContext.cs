﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqProject.Model;

namespace LinqProject.Data
{
    public class MyAppContext : DbContext
    {
        public MyAppContext() : base("name=DbConnection")
        {
          
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<History> History { get; set; }

     
    }

}
