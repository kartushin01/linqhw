﻿using LinqProject.Model;

namespace LinqProject.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<LinqProject.Data.MyAppContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(LinqProject.Data.MyAppContext context)
        {
            context.Users.AddOrUpdate(x => x.Id,
                new User()
                {
                    Id = 1,
                    FirstName = "Jhon",
                    LastName = "Conor",
                    Login = "jhon@mail.com",
                    PassportData = 123456,
                    Password = "123",
                    PhoneNumber = 1234567890,
                    RegistrationDate = DateTime.Today.AddDays(-2)
                },
                new User()
                {
                    Id = 2,
                    FirstName = "Sara",
                    LastName = "Conor",
                    Login = "Sara@mail.com",
                    PassportData = 123456,
                    Password = "321",
                    PhoneNumber = 0987654321,
                    RegistrationDate = DateTime.Today.AddDays(-1)
                }, new User()
                {
                    Id = 3,
                    FirstName = "Optimus",
                    LastName = "Prime",
                    Login = "Optimus@mail.com",
                    PassportData = 123456,
                    Password = "012",
                    PhoneNumber = 111222333,
                    RegistrationDate = DateTime.Now
                }
            );

            context.Accounts.AddOrUpdate(x => x.Id,

                new Account()
                {
                    Id = 1,
                    AccountOpen = DateTime.Now,
                    Total = 0,
                    UserId = 1
                },

                new Account()
                {
                    Id = 2,
                    AccountOpen = DateTime.Now,
                    Total = 0,
                    UserId = 1
                },
                new Account()
                {
                    Id = 3,
                    AccountOpen = DateTime.Now,
                    Total = 0,
                    UserId = 2
                },
                new Account()
                {
                    Id = 4,
                    AccountOpen = DateTime.Now,
                    Total = 0,
                    UserId = 3
                },
                new Account()
                {
                    Id = 5,
                    AccountOpen = DateTime.Now,
                    Total = 0,
                    UserId = 3
                }
            );

           
        }
    }
}
