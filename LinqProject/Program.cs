﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using LinqProject.Data;
using LinqProject.Model;

namespace LinqProject
{
    class Program
    {
        static void Main(string[] args)
        {
            string login, password;
            ShowMenu();
        }

        private static void ShowMenu()
        {
            Console.WriteLine("Меню:");
            Console.WriteLine("Нажмите 1 - Список юзеров:");
            Console.WriteLine("Нажмите 2 - Вход в систему:");
            Console.WriteLine("Нажмите 3 - Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта:");
            Console.WriteLine("Нажмите 4 - Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)");

            switch (Console.ReadLine())
            {
                case "1":
                    PrintUserList();
                    break;
                case "2":
                    Login();
                    break;
                case "3":
                    ShowAllPlus();
                    break;
                case "4":
                    ShowUsersSumN();
                    break;
            }
        }

        /// <summary>
        ///  1. Вывод информации о заданном аккаунте по логину и паролю
        ///  2. Вывод данных о всех счетах заданного пользователя
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        private static void GetAccountWithHistory(string login, string password)
        {
            using (var context = new MyAppContext())
            {
                var user = context.Users.Include(d => d.Accounts.Select(h => h.History)).FirstOrDefault(l => l.Login == login && l.Password == password);

                Console.WriteLine($"id: {user.Id}; Имя: {user.FirstName}; Фамилия: {user.LastName}; Логин:{user.Login}; Пароль:{user.Password}");

                Console.WriteLine("Счета пользователя:");

                foreach (var acc in user.Accounts)
                {
                    Console.WriteLine($"id: {acc.Id}; дата открытия:{acc.AccountOpen}; Остаток: {acc.Total}");

                    Console.WriteLine("Операции по счёту:");

                    foreach (var h in acc.History)
                    {
                        var val = h.TypeOperation;
                        var enumDisplayStatus = (TypeOperation)val;
                        Console.WriteLine($"№{h.Id} | Сумма :{h.Sum} | Тип транзакции :{enumDisplayStatus} ");
                    }
                }

                ShowAccountMenu(user);
            }
             
        }

        /// <summary>
        /// 3. Вывод данных о конкретном счете заданного пользователя, включая историю по каждому счёту
        /// </summary>
        /// <param name="accoutId"></param>
        private static void GetAccount(int accoutId)
        {
            using (var context = new MyAppContext())
            {
                var acc = context.Accounts.Include(h => h.History).FirstOrDefault(a => a.Id == accoutId);

                Console.WriteLine($"Счёт № {acc.Id} | Открыт :{acc.AccountOpen} | Остаток:{acc.Total} ");

                foreach (var h in acc.History)
                {
                    var val = h.TypeOperation;
                    var enumDisplayStatus = (TypeOperation)val;
                    Console.WriteLine($"№{h.Id} | Сумма :{h.Sum} | Тип транзакции :{enumDisplayStatus} ");
                }

            }
            Console.WriteLine("Обратно в меню намите 0");
            Console.WriteLine("Совершить операцию по счёту - 1");

            var userSelect = Convert.ToInt32(Console.ReadLine());
            
            switch (userSelect)
            {
                case 0:
                    ShowMenu();
                    break;
                case 1:
                    AccountOperationsMenu(accoutId);
                    break;
            }
        }
        /// <summary>
        /// метод пополнения или списания средств
        /// </summary>
        /// <param name="accoutId"></param>
        private static void AccountOperationsMenu(int accoutId)
        {
            Console.WriteLine("Пополнить аккаунт - 1");
            Console.WriteLine("Списать срдества -  2");

            int userSelect = Convert.ToInt32(Console.ReadLine());

            TypeOperation typeoperation = (TypeOperation) 0;

            switch (userSelect)
            {
                case 1:
                    typeoperation = TypeOperation.Plus;
                    break;
                case 2:
                    typeoperation = TypeOperation.Minus;
                    break;
            }

            Console.WriteLine($"Введите сумму:");
            
            decimal OprationSum = Convert.ToDecimal(Console.ReadLine());

            History opretaion = new History()
            {
                Dt = DateTime.Now,
                Sum = OprationSum,
                TypeOperation = typeoperation,
                AccountId = accoutId
            };

            using (var context = new MyAppContext())
            {
                context.History.Add(opretaion);
                context.SaveChanges();

                var acc =
                    context.Accounts.FirstOrDefault(i => i.Id == accoutId);

                if (typeoperation == TypeOperation.Plus)
                {
                    acc.Total += OprationSum;
                }
                else if (typeoperation == TypeOperation.Minus)
                {
                    acc.Total -= OprationSum;
                }
                context.SaveChanges();

                Console.WriteLine($"Операция проведена!");

                GetAccount(accoutId);
            }
            

        }

        /// <summary>
        /// 4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта
        /// </summary>
        private static void ShowAllPlus()
        {
            using (var context = new MyAppContext())
            {
                var operations = context.History.Include(u => u.Account.User).Where(t=>t.TypeOperation == TypeOperation.Plus).ToList();

                Console.WriteLine($"Все операции пополенения:");

                foreach (var o in operations)
                {
                    Console.WriteLine($"id : {o.Id} | Сумма : {o.Sum} | Владелец : {o.Account.User.FirstName} {o.Account.User.LastName} ");
                }

                ShowMenu();

            }
        }

        /// <summary>
        /// 5. Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой)
        /// </summary>
        
        private static void ShowUsersSumN()
        {
            Console.WriteLine($"Введите сумму:");
            
            decimal userSum = Convert.ToDecimal(Console.ReadLine());
            
            using (var context = new MyAppContext())
            {
                IQueryable<IGrouping<string, decimal>> res = context.Accounts.Where(t => t.Total >= userSum).GroupBy(acc=>acc.User.Login, acc=>acc.Total);

                foreach (var r in res)
                {
                    Console.WriteLine(r.Key);

                    foreach (decimal total in r)
                        Console.WriteLine("  {0}", total);
                }
            }

            ShowMenu();
        }


        private static void ShowAccountMenu(User user)
        {
            Console.WriteLine("Войти в аккаунт введите его id");
            Console.WriteLine("Выход в главное меню - нажмите 0");

            var userSelect = Convert.ToInt32(Console.ReadLine());

            if (user.Accounts.Any(i => i.Id == userSelect))
            {
                GetAccount(userSelect);
            }
            else if (userSelect == 0)
            {
                ShowMenu();
            }
            else
            {
                Console.WriteLine("Ошибка попробуйте снова");
                ShowAccountMenu(user);
            }
        }

        
        /// <summary>
        /// Все юзеры
        /// </summary>
        private static void PrintUserList()
        {
            using (var context = new MyAppContext())
            {
                var users = context.Users.ToList();

                foreach (var user in users)
                {
                    Console.WriteLine($"{user.Id}|{user.FirstName}|{user.LastName}|{user.Login}|{user.Password}");
                }
            }

            ShowMenu();
        }

        private static void Login()
        {
            string login;
            string password;

            Console.WriteLine($"Введите логин");
            login = Console.ReadLine();

            if (!CheckLogin(login))
            {
                Console.WriteLine($"Ошибка");
                ShowMenu();
            }
            else
            {
                Console.WriteLine($"Введите пароль");
                password = Console.ReadLine();
                bool resAuth = CheckLoginAndPassword(login, password);
                if (!resAuth)
                {
                    Console.WriteLine($"Неверный пароль");
                    Console.WriteLine($"Намите 1 - назад в меню");
                    Console.WriteLine($"Намите 2 - ввести данные заново");

                    switch (Console.ReadLine())
                    {
                        case "1":
                            PrintUserList();
                            break;
                        case "2":
                            Login();
                            break;
                    }
                    password = Console.ReadLine();
                }
                else
                {
                    GetAccountWithHistory(login, password);
                }
            }

        }
        private static bool CheckLogin(string login)
        {
            using (var context = new MyAppContext())
            {
                return context.Users.Any(l => l.Login == login);
            }
        }
        private static bool CheckLoginAndPassword(string login, string password)
        {
            using (var context = new MyAppContext())
            {
                return context.Users.Any(l => l.Login == login && l.Password == password);
            }
        }


    }
}
