﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqProject.Model
{
    public class Account
    {
        public int Id { get; set; }
        public DateTime AccountOpen { get; set; }
        public decimal Total { get; set; }

        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public  ICollection<History> History {get; set; }  
    }
}
