﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqProject.Model
{
    public class History
    {
        public int Id { get; set; }
        public decimal Sum { get; set; }
        public DateTime Dt { get; set; }
        public int AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public TypeOperation TypeOperation { get; set; }
    }

    public enum TypeOperation
    {
        Plus = 1,
        Minus = 2
    }
}
